//
//  NewsFeedCollectionViewCell.swift
//  DemoCollectionView
//
//  Created by Payal Rajput on 07/05/20.
//  Copyright © 2020 Nilisha Gupta. All rights reserved.
//

import UIKit

class NewsFeedCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageViewPost: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPublishedAt: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(with image: UIImage) {
        imageViewPost.image = image
    }

}
