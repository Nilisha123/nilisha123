//
//  ViewController.swift
//  DemoCollectionView
//
//  Created by Payal Rajput on 07/05/20.
//  Copyright © 2020 Nilisha Gupta. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: - Outlet declaration
    @IBOutlet weak var collectionViewNewsFeed: UICollectionView!
    
    // MARK: - Declaration of Variables & Constants
    let cellIdentifier = "NewsFeedCollectionViewCell"
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUp()
    }
    
    func setUp() {
        let nib = UINib(nibName: "NewsFeedCollectionViewCell", bundle: nil)
        collectionViewNewsFeed.register(nib, forCellWithReuseIdentifier: cellIdentifier)
    }
    
}

extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 14
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? NewsFeedCollectionViewCell else {
            return UICollectionViewCell()
        }
        if let image = UIImage(named: "alia") {
            cell.configureCell(with: image)
            return cell
        }
        return cell
    }
    
}

extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionViewNewsFeed.deselectItem(at: indexPath, animated: true)
        print("Tapped")
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    
}

